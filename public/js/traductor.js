var dataLanguage = {
    title0: {
        es: "Cutting Edge Technologies for Realities",
        en: "Cutting Edge Technologies for Realities",
        placeholder: NaN,
    },
    title1: {
        es: "Cutting Edge Technologies for Realities",
        en: "Cutting Edge Technologies for Realities",
        placeholder: NaN,
    },
    menu1: { es: "INICIO", en: "HOME", placeholder: NaN },
    menu2: { es: "NOSOTROS", en: "ABOUT", placeholder: NaN },
    menu3: { es: "PRODUCTOS", en: "PORTFOLIO", placeholder: NaN },
    menu4: { es: "SERVICIOS", en: "SERVICES", placeholder: NaN },
    menu5: { es: "CONTACTO", en: "CONTACT", placeholder: NaN },
    title2: {
        es: "\u00bfQu\u00e9 realidad quieres experimentar?",
        en: "What reality do you want to experience?",
        placeholder: NaN,
    },
    button1: { es: "DEMOS", en: "DEMOS", placeholder: NaN },
    title3: {
        es: "Valor diferencial",
        en: "Differential value",
        placeholder: NaN,
    },
    text1: {
        es:
            "Servicios de dise\u00f1o y desarrollo de proyectos que emplean de tecnolog\u00eda de realidad mixta e inteligencia artificial. Facilitamos la transformaci\u00f3n digital y la incorporaci\u00f3n tecnologias de cuarta revoluci\u00f3n industrial en el n\u00facleo de negocios de nuestros clientes.",
        en:
            "\nServices for designing and developing of projects that apply mixed reality technology and artificial intelligence. We facilitate the digital transformation and the transformation of technologies of the fourth industrial revolution in the core of our clients' businesses",
        placeholder: NaN,
    },
    title4: { es: "Sobre nosotros", en: "About us", placeholder: NaN },
    text2: {
        es:
            "Somos un equipo de ingenieros, dise\u00f1adores y amantes de la tecnolog\u00eda que dise\u00f1an y crean soluciones con tecnolog\u00edas de vanguardia para diferentes realidades.",
        en:
            "We are a team of engineers, designers and technology lovers who design and create solutions with cutting-edge technologies for different realities.",
        placeholder: NaN,
    },
    title5: { es: "PRODUCTOS", en: "PORTFOLIO", placeholder: NaN },
    portfolioText1: {
        es:
            "Aplicaci\u00f3n de realidad mixta, que optimiza la administraci\u00f3n de recursos de un hospital y la visualizaci\u00f3n de datos en tiempo real",
        en:
            "Mixed reality application, which optimizes the management of hospital resources and the visualization of data in real time",
        placeholder: NaN,
    },
    portfolioText2: {
        es:
            "Realidad mixta aplicada al sector justicia, permite visualizaci\u00f3n de datos, registro de eventos, administraci\u00f3n de tareas, desarrollada para uso en juzgados",
        en:
            "Mixed reality applied to the justice sector, allows data visualization, event registration, task administration, developed for use in courts",
        placeholder: NaN,
    },
    portfolioText3: {
        es:
            "Aplicaci\u00f3n de realidad mixta para visualizaci\u00f3n de datos en buques, mantenimiento preventivo y configuraci\u00f3n de alertas",
        en:
            "Mixed reality application for data visualization on ships, preventive maintenance and alert configuration.",
        placeholder: NaN,
    },
    portfolioText4: {
        es:
            "Producto de realidad mixta orientado al mantenimiento en entornos industriales, mantenimiento preventivo y entrenamiento.",
        en:
            "Mixed reality product oriented to maintenance in industrial environments, preventive maintenance of equipment and staff training.",
        placeholder: NaN,
    },
    portfolioText5: {
        es:
            "Aplicaci\u00f3n desarrollada para contactar t\u00e9cnicos de mantenimiento de electrodom\u00e9sticos con usuarios con equipos que presentan fallos.",
        en:
            "Application developed to contact appliance maintenance technicians with users who have equipment that require be fixed. ",
        placeholder: NaN,
    },
    portfolioText6: {
        es:
            "Aplicaci\u00f3n de realidad mixta para realizar compras \u00e1giles sin contacto con los productos",
        en:
            "Mixed reality application to make agile purchases without contact with products",
        placeholder: NaN,
    },
    services1: {
        es: "DISE\u00d1O DE EXPERIENCIAS EN AR/VR/XR/MR",
        en: "DESIGN OF  AR / VR / XR / MR EXPERIENCE",
        placeholder: NaN,
    },
    services2: {
        es: "GRANDES DATOS E INTELIGENCIA ARTIFICIAL",
        en: "BIG DATA AND ARTIFICIAL INTELLIGENCE",
        placeholder: NaN,
    },
    services3: {
        es: "DESARROLLO WEB Y M\u00d3VIL",
        en: "WEB AND MOBILE DEVELOPMENT ",
        placeholder: NaN,
    },
    services4: {
        es: "IOT Y COMPUTACI\u00d3N EN LA NUBE",
        en: "IOT AND CLOUD COMPUTING",
        placeholder: NaN,
    },
    other1: { es: "Hecho con", en: "Made with", placeholder: NaN },
    title6: { es: "Cont\u00e1ctanos", en: "Get In Touch", placeholder: NaN },
    title7: {
        es: "Env\u00edanos un mensaje",
        en: "Send us a Message",
        placeholder: NaN,
    },
    contact1: { es: "Nombre Completo", en: "Full Name", placeholder: true },
    contact2: { es: "Correo Electr\u00f3nico", en: "Email", placeholder: true },
    contact3: { es: "Asunto", en: "Subject", placeholder: true },
    contact4: { es: "Empresa", en: "Company", placeholder: true },
    contact5: { es: "Mensaje", en: "Your Message", placeholder: true },
    contact6: { es: "Enviar", en: "Submit", placeholder: NaN },
    title8: {
        es: "Datos de Contacto",
        en: "Contact Details",
        placeholder: NaN,
    },
    title9: { es: "SERVICIOS", en: "SERVICES", placeholder: NaN },
};

function changeLanguage(lang) {
    Object.entries(dataLanguage).forEach(([key, value]) => {
        console.log(key, value);
        try {
            if (value["placeholder"] === true) {
                document.getElementById(key).placeholder = value[lang];
            } else {
                document.getElementById(key).innerHTML = value[lang];
            }
        } catch (error) {
            console.log("Error es id inexistente", error, key);
        }
    });
}
changeLanguage("en");
