// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyAnGyaeJIfnTglnP3YOmEPqq2-h-_nN520",
    authDomain: "ibm-hack-1ee6d.firebaseapp.com",
    databaseURL: "https://ibm-hack-1ee6d.firebaseio.com",
    projectId: "ibm-hack-1ee6d",
    storageBucket: "ibm-hack-1ee6d.appspot.com",
    messagingSenderId: "661026893033",
    appId: "1:661026893033:web:ee742164c98e39d0",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

function sendTo() {
    var postData = {
        fullNameField: document.getElementById("contact1").value,
        mailField: document.getElementById("contact2").value,
        contactUsField: document.getElementById("contact3").value,
        companyField: document.getElementById("contact4").value,
        userMsgField: document.getElementById("contact5").value,
        date: Date(),
    };
    console.log(postData);
    firebase
        .database()
        .ref()
        .child("contact-us")
        .push(postData)
        .then(function () {
            console.log("Save!!");
            alert("Thank you. We will contact you as soon as possible.");
            document.getElementById("contact1").value = "";
            document.getElementById("contact2").value = "";
            document.getElementById("contact3").value = "";
            document.getElementById("contact4").value = "";
            document.getElementById("contact5").value = "";
        });
}
